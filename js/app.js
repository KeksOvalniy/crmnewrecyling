$(document).ready(function(){

    $('.menu-item').on('click',function(){
        $(this).addClass('menu-item__active').siblings().removeClass('menu-item__active')
        
    })
    

    // Открытие/закрытие окна уведомлений
    $('.navbar-icon__alerts').on('click', function(){
        $('.navbar-modal__alerts').fadeToggle()
    })

    // Переключение кнопок "Рядом", "Работают"
    $('.btn-toggle').on('click', function(){
        $(this).addClass('btn-toggle__active').siblings().removeClass('btn-toggle__active')
    })

    // Открытие/закрытие формы
    $('.content-control__create, .form-create__close').on('click', function(){
        $('.content-control__add').fadeToggle()
    })

    // Добавление в избранное
    $('.fa-star').on('click', function(){
       if($(this).hasClass('fal')){
           $(this).removeClass('fal').addClass('fas')
       }else{
           $(this).removeClass('fas').addClass('fal')
       }            
    })

    // Появление кнопки "Удалить" при выборе итема
    $('.content-table__checkbox input').on('click', function(){
        $('.btn-delete').toggleClass('btn-delete__active')
    })

    // Переход в форме добавления объекта
    $('.form-create__next').on('click', function(){
        $('.form-create__tabs').find('.form-create__tab-active').removeClass('form-create__tab-active').next().addClass('form-create__tab-active')
        $('.form-create__content').find('.form-create__item-active').removeClass('form-create__item-active').next().addClass('form-create__item-active')
        if($('.form-create__social').hasClass('form-create__item-active')){
            $('.form-create__next').html('Добавить объект').addClass('form-create__next-add')
        }
        $('.form-create__next-add').on('click', function(){
            $('.content-control__add').fadeOut()
        })
    })
   
    

})